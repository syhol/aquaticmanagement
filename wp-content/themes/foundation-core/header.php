<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	
<div id="page" class="hfeed site">
	
	<header class="site-header row">
		
		<hgroup class="site-header column small-12">
			
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			
			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
			
		</hgroup>

		<div class="column small-12">

			<nav class="site-navigation top-bar">
				
				<ul class="title-area">
						
					<li class="name">
						<h1><a href="#">Top Bar Title </a></h1>
					</li>
					
					<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
					
				</ul>
				
				<section class="top-bar-section">
					
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'left', 'container' => false,) ); ?>
					
					
					<form class="site-search right" action="/">
			        	<input type="text" name="s" class="small-8 columns" placeholder="Search">
			        	<input type="submit" class="small-4 columns button prefix" value="Search">
			        </form>
	          		
	        	</section>
				
			</nav><!-- .site-navigation -->
		
		</div>
		
	</header><!-- .site-header -->

	<div class="wrapper row">
		