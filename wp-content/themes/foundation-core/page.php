<?php get_header(); ?>

<div class="site-content column large-7">
		
	<div id="content" role="main">
			
		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>
				
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<header class="entry-header">
					<h1 class="entry-title"><?php _e( get_the_title() , 'twentytwelve' ); ?></h1>
				</header>

				<div class="entry-content">
					<? the_content(); ?>
				</div><!-- .entry-content -->

			</article><!-- #post-0 -->
				
			<?php endwhile; ?>

		<?php else : ?>

			<article id="post-0" class="post no-results not-found">

			
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentytwelve' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'twentytwelve' ); ?></p>
					<?php get_search_form(); ?>
				</div><!-- .entry-content -->

			</article><!-- #post-0 -->

		<?php endif; // end have_posts() check ?>

		</div><!-- #content -->
		
	</div><!-- .site-content-->
	
<?php get_sidebar(); ?>
<?php get_footer(); ?>